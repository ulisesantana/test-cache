# Test cache and distributed lock

 Requirements for the environment:

- Redis
- MySQL

When you have the environment ready make a copy of config files and fill them.

For the testing MySQL database just run this code on the console:

`mysql -u root -p < path/to/test.database.sql`


## Run

Open 3 terminals and execute in each one just one of this commands: 

 - `npm run start`
 - `npm run start2`
 - `npm run start3`

 With the 3 servers up and running just open the `index.html` file at the root of the repo.

 ![Running example](docs/screenshot.png)