'use strict';

const logger = require('./lib/logger');
const restify = require('restify');
const apiCache = require('./src/cache/cache.router');
const port = process.env.PORT || 8080

let server = restify.createServer();

server.use(restify.acceptParser(server.acceptable));
server.use(restify.queryParser());
server.use(restify.bodyParser());

// Configurating CORS (Cross-Origin Resource Sharing)
server.use( (req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',
        'X-API-KEY, Origin, X-Requested-With, Content-Type,'+
        ' Accept, Access-Control-Request-Method' );
    res.header('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, DELETE');
    res.header('Allow', 'GET, POST, OPTIONS, PUT, DELETE');

    next();
});

apiCache(server);

server.listen(port, () => logger.info(`Listening at ${server.url}`));
