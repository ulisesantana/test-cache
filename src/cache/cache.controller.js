'use strict'

const logger = require('../../lib/logger');
const redis = require('redis');
const bluebird = require('bluebird');
const redisConfig = require('../../config/redis.config');
const Redlock = require('redlock');
const appName = require('../../package.json').name;

const mysql = require('mysql');
const mysqlConfig = require('../../config/mysql.config');
const connection = mysql.createConnection(mysqlConfig);

bluebird.promisifyAll(redis.RedisClient.prototype);
bluebird.promisifyAll(redis.Multi.prototype);

const client = redis.createClient(redisConfig[0].port, redisConfig[0].host);
client.on('connect', () => logger.info(`Successful redis connection from ${appName}`));

const redlock = new Redlock([client], {
  driftFactor: 0.1,
  retryCount: 3,
  retryDelay: 200,
  retryJitter: 200
});

function getCachedData(req, res) {
  client.lrange('users', 0, -1, (err, object) => {
    let objectCollection = [];

    Object.keys(object).forEach((key) => {
      objectCollection.push(JSON.parse(object[key]));
    });
    logger.debug('Sending data through cache')
    res.send({ objectCollection });
  });
}

function getData(req, res) {
  client.getAsync('app:feature:lock:expireTime').then((expireTime) => {
    if (expireTime <= Date.now()) {
      updateCacheData(req, res);
    }
    else {
      getCachedData(req, res);
    }
  });
}

function updateCacheData(req, res) {
  redlock.lock('app:feature:lock', 1000).then(lock => {
    client.set('app:feature:lock:expireTime', Date.now() + (5 * 1000));
    client.del('users');

    connection.query('SELECT * from users', (error, results) => {
      if (error) throw error;
      for (let result of results) {
        client.rpush(['users', JSON.stringify(result)]);
      }
      logger.info('Cache updated');
      getCachedData(req, res);
    });
    return lock.unlock()
      .catch(function (err) {
        logger.error(err.message);
      });
  }).catch((err) => {
    logger.error(err.message);
    getCachedData(req, res);
  });
}

module.exports = getData;