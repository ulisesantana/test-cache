'use strict';

const restify = require('restify');
const _ctrl = require('./cache.controller');

module.exports = server => {
  server.get('/cache', _ctrl);
};