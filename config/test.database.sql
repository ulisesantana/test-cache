CREATE DATABASE test;

USE test;

CREATE TABLE users (name varchar(20), email varchar(50), phone varchar(12));

INSERT INTO test.users (name, email, phone) VALUES ("ulises", "ulisesantana@gmail.com", "612456789"), 
("alba", "alba@gmail.com", "645678912"), ("ayoze", "ayoze@gmail.com", "678123546") ;
