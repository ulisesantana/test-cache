'use strict';

/**
 * Logger. This module is an instance of {@link https://www.npmjs.com/package/winston|winston}.
 * @module logger
 */

let appName = require('./../package.json').name;
let winston = require('winston');
require('winston-logstash');
let os = require('os');

// Instantiate logger
let logger = module.exports = new winston.Logger();

// Add console transport
logger.add(winston.transports.Console, {
  colorize: true,
  level: process.env.NODE_ENV === 'production' ? 'info' : 'debug',
  timestamp: function () {
    return new Date().toUTCString();
  },
  stderrLevels: process.env.NODE_ENV === 'production' ?
    ['error', 'debug'] :
    ['error'],
  formatter: function (options) {
    let date = options.timestamp();
    let level = winston.config.colorize(options.level, options.level);
    let stack = (options.meta && options.meta.stack) ? options.meta.stack : '';
    let msg = options.message ? options.message : '';
    let showMeta = options.level === 'debug' || options.level === 'silly';
    msg = stack ?
      `${msg} ${stack}` :
      (showMeta ? `${msg} ${JSON.stringify(options.meta)}` : msg);
    return `${date} - ${level}: ${msg}`;
  }
});